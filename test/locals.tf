# root/locals.tf

locals {
  certificate_domains = {
    mosar-inquisitive-test = {
      domain            = "mosar-test.inquisitive.nl"
      validation_method = "EMAIL"
    }
    mosar-inquisitive-test-feature-flags = {
      domain            = "mosar-test-feature-flags.inquisitive.nl"
      validation_method = "EMAIL"
    }

    mosar-jenkins = {
      domain            = "jenkins.inquisitive.nl"
      validation_method = "EMAIL"
    }
  }
}

