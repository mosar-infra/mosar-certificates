# root/main.tf

module "certificates" {
  source                    = "git::https://gitlab.com/mosar-infra/tf-module-certificates.git?ref=tags/v1.0.2"
  environment               = var.environment
  certificate_domains       = local.certificate_domains
}
