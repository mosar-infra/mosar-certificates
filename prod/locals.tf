# root/locals.tf

locals {
  certificate_domains = {
    mosar-inquisitive-prod = {
      domain            = "mosar.inquisitive.nl"
      validation_method = "EMAIL"
    }
    mosar-inquisitive-prod-feature-flags = {
      domain            = "mosar-${var.environment}-feature-flags.inquisitive.nl"
      validation_method = "EMAIL"
    }
  }
}

