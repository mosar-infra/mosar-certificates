output "certificates" {
  value     = module.certificates.certificates
  sensitive = true
}
